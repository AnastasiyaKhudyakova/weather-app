const CracoLessPlugin = require('craco-less');

module.exports = {
	plugins: [
		{
			plugin: CracoLessPlugin,
			options: {
				lessLoaderOptions: {
					lessOptions: {
						modifyVars: {
							'@font-size-base': '13.5px',
							'@font-family': 'Maven Pro, sans-serif',
							'@card-padding-base': '1em',
							'@divider-text-padding': '0.5em',
							'@primary-color': '#04A777',
							'@info-color': '#43AA8B',
							'@success-color': '#04A777',
							'@processing-color': '#6E162B',
							'@error-color': '#DB504A',
							'@highlight-color': '#FCDC4D',
							'@warning-color': '#FCDC4D',
							'@normal-color': '#d9d9d9',
							'@body-background': '#112123',
							'@component-background': '#112123',
							'@text-color': '#FBF2C0',
							'@border-radius-base': '0px',
							'@disabled-bg': 'fade(#FBF2C0, 70%)',
							'@tabs-card-head-background': 'fade(#112123, 50%)'
						},
						javascriptEnabled: true,
					},
				},
			},
		},
	]
};

