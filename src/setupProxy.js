/* eslint-disable no-undef */
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = (app) => {
  app.use(
    '/api/lng-detector**',
    createProxyMiddleware({
      target: 'https://ws.detectlanguage.com/0.2/detect?',
      changeOrigin: true,
      secure: false,
    }),
  );
  app.use(
    '/api/yandex**',
    createProxyMiddleware({
      target: 'https://api.weather.yandex.ru/v2/forecast?',
      changeOrigin: true,
      secure: false,
      headers: {
        'Content-type': 'application/json',
      },
    }),
  );
};
