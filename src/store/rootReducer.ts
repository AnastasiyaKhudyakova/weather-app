import { combineReducers } from 'redux';
import { authSlice } from './auth/reducer';
import { userConfigSlice } from './userConfig/reducer';

const rootReducer = combineReducers({
  auth: authSlice.reducer,
  userConfig: userConfigSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
