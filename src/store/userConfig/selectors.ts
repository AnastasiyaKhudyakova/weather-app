import { createSelector } from 'reselect';
import { RootState } from '../rootReducer';
import { IUserWeatherData, IUserTabConfigs } from './reducer';

const selectUserConfigStatus = (state: RootState) => state.userConfig.isSaved;

const selectUserConfig = (state: RootState) => state.userConfig.config;

const selectUserConfigTab = (tabIndex: number) =>
  createSelector(selectUserConfig, (config: IUserTabConfigs[]) => config[tabIndex].weather);

const selectWeatherByServiceName = (tabIndex: number, serviceName: string) =>
  createSelector(
    selectUserConfigTab(tabIndex),
    (weatherTab: IUserWeatherData) => weatherTab[serviceName],
  );

export {
  selectUserConfigTab,
  selectUserConfig,
  selectUserConfigStatus,
  selectWeatherByServiceName,
};
