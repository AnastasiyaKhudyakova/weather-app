import { createReducer, createSlice } from '@reduxjs/toolkit';
import { saveUserConfig } from './actions';
import { initWeatherState, IWeatherState } from '../types';
import { fetchUserConfig } from './actions';
import { setTab, addTab, removeTab, updateTab, refreshTab } from './types';

export interface IUserWeatherData {
  [service: string]: IWeatherState;
}

export interface IUserTabConfigs {
  lon: number;
  lat: number;
  city: string;
  weather: IUserWeatherData;
}

export interface IUserConfigState {
  config: IUserTabConfigs[];
  isSaved: boolean;
}

const initialState: IUserConfigState = {
  config: [],
  isSaved: true,
};

const configDefaultValue = [
  {
    lon: 42.9837,
    lat: 81.2497,
    city: 'London',
    weather: {
      weatherMap: { ...initWeatherState },
    },
  },
];

const userConfigReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(setTab, (state, action) => {
      const { tabIndex, tabValue } = action.payload;
      const { weather } = state.config[tabIndex];
      const updatedWeather = { ...weather, ...tabValue };

      state.config[tabIndex].weather = updatedWeather;
    })
    .addCase(updateTab, (state, action) => {
      const { tabIndex, tabValue } = action.payload;

      const currentTabValue = state.config[tabIndex];
      const newValue = { ...currentTabValue, ...tabValue };

      state.config[tabIndex] = newValue;
      state.isSaved = false;
    })
    .addCase(addTab, (state, action) => {
      state.config.push({ ...configDefaultValue[0] });
      state.isSaved = false;
    })
    .addCase(refreshTab, (state, action) => {
      const { tabIndex, serviceName } = action.payload;
      const currentTabValue = state.config[tabIndex];

      if (serviceName) {
        state.config[tabIndex].weather[serviceName] = { ...initWeatherState };

        return;
      }

      const refreshedWeatherData = Object.keys(currentTabValue.weather).reduce<IUserWeatherData>(
        (weatherData, tabValue) => {
          weatherData[tabValue] = { ...initWeatherState };

          return weatherData;
        },
        {},
      );

      state.config[tabIndex] = { ...currentTabValue, weather: refreshedWeatherData };
    })
    .addCase(removeTab, (state, action) => {
      const index = action.payload;

      if (state.config.length > 1) {
        state.config.splice(index, 1);
        state.isSaved = false;
      }
    });
});

const userConfigSlice = createSlice({
  name: 'userConfig',
  initialState: initialState,
  reducers: {
    setWeather: userConfigReducer,
    update: userConfigReducer,
    addTab: userConfigReducer,
    removeTab: userConfigReducer,
    refreshTab: userConfigReducer,
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUserConfig.fulfilled, (state, action) => {
        state.config = action.payload.length ? action.payload : configDefaultValue;
        state.isSaved = true;
      })
      .addCase(fetchUserConfig.rejected, (state, action) => {
        state.config = configDefaultValue;
        state.isSaved = true;
      })
      .addCase(saveUserConfig.fulfilled, (state, action) => {
        state.isSaved = true;
      })
      .addCase(saveUserConfig.rejected, (state, action) => {
        state.isSaved = false;
      });
  },
});

export { userConfigSlice };
