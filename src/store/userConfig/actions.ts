import { createAsyncThunk, ThunkDispatch } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import { getUser, IUserReqParams, updateUser } from '../../api/users';
import { WeatherApis } from '../../App';
import { ICoords } from '../../pages/Main/Main';
import { getClimacellWeather } from '../../services/climacell';
import { getWBitWeather } from '../../services/weatherBit';
import { getOpenMapWeather } from '../../services/weatherMap';
import { getYandexWeather } from '../../services/yandex';
import { RootState } from '../rootReducer';
import { initWeatherState } from '../types';
import { IUserTabConfigs, IUserWeatherData } from './reducer';
import { setTab, updateTab, removeTab, addTab, ITabUpdateParams } from './types';
import { AppDispatch } from '../store';

export interface ITabWeatherReqData {
  serviceName: string;
  coords: ICoords;
}

export interface IWeatherRequestParams {
  apiName: string;
  tabIndex: number;
  coords: ICoords;
}

export const WApiMap = (apiName: string) => {
  switch (apiName) {
    case WeatherApis.CLIMACELL:
      return (coords: ICoords) => getClimacellWeather(coords);
    case WeatherApis.WEATHER_BIT:
      return (coords: ICoords) => getWBitWeather(coords);
    case WeatherApis.YANDEX:
      return (coords: ICoords) => getYandexWeather(coords);
    case WeatherApis.WEATHER_MAP:
      return (coords: ICoords) => getOpenMapWeather(coords);
    default:
      return (coords: ICoords) => getOpenMapWeather(coords);
  }
};

const fetchUserConfig = createAsyncThunk<
  IUserTabConfigs[],
  IUserReqParams,
  {
    state: RootState;
  }
>('userConfig/fetch', async (params, thunkAPI) => {
  const response = await getUser(params);
  let weatherData: IUserTabConfigs[] = [];

  if (response.data?.config) {
    weatherData = response.data.config.map((tabConfig: IUserTabConfigs) => {
      const data = { ...tabConfig };

      Object.keys(data.weather).forEach((serviceName: string) => {
        if (data.weather[serviceName].lastFetched + 3600000 < new Date().getTime()) {
          data.weather[serviceName].isFetching = true;
        }
      });

      return data;
    });
  }

  return weatherData;
});

const saveUserConfig = createAsyncThunk<
  AxiosResponse,
  void,
  {
    state: RootState;
  }
>('userConfig/save', async (args, thunkAPI) => {
  const { userConfig, auth } = thunkAPI.getState();

  if (auth.user) {
    const reqParam: IUserReqParams = {
      uid: auth.user.uid,
      idToken: auth.user.idToken,
      data: { config: userConfig.config },
    };

    const response = await updateUser(reqParam);

    return response;
  }

  return Promise.reject('Error');
});

const getTabWeather = createAsyncThunk<
  void,
  IWeatherRequestParams,
  {
    state: RootState;
  }
>(
  'userConfig/getWeather',
  async (params, thunkAPI) => {
    const { apiName, tabIndex, coords } = params;

    const getWeather = WApiMap(apiName);
    const weatherTab: IUserWeatherData = {};

    weatherTab[apiName] = { ...initWeatherState };

    try {
      const weatherData = await getWeather(coords);

      weatherTab[apiName].weatherData = weatherData;
      weatherTab[apiName].lastFetched = new Date().getTime();
    } catch (error) {
      weatherTab[apiName].error = error;
    }

    weatherTab[apiName].isFetching = false;
    thunkAPI.dispatch(setTab({ tabIndex, tabValue: weatherTab }));
    thunkAPI.dispatch(saveUserConfig());
  },
  {
    condition: (params, { getState, extra }) => {
      const { apiName, tabIndex } = params;

      const { config } = getState().userConfig;

      const fetchTimestamp = config[tabIndex].weather?.[apiName]?.lastFetched;
      const shouldUpdate = fetchTimestamp && fetchTimestamp + 3600 > new Date().getTime();

      return !shouldUpdate;
    },
  },
);

const removeWeatherTab = (index: number) => (dispatch: AppDispatch | any) => {
  dispatch(removeTab(index));
  dispatch(saveUserConfig());
};

const addWeatherTab = () => (dispatch: AppDispatch | any) => {
  dispatch(addTab());
  dispatch(saveUserConfig());
};

const updateWeatherTab = (updateParams: ITabUpdateParams) => (dispatch: AppDispatch | any) => {
  dispatch(updateTab(updateParams));
  dispatch(saveUserConfig());
};

export {
  getTabWeather,
  fetchUserConfig,
  saveUserConfig,
  removeWeatherTab,
  addWeatherTab,
  updateWeatherTab,
};
