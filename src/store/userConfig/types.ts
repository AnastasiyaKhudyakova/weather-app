import { createAction } from '@reduxjs/toolkit';
import { IUserWeatherData, IUserTabConfigs } from './reducer';

export interface ITabUpdateParams {
  tabValue: IUserTabConfigs;
  tabIndex: number;
}

export interface ITabRefreshParams {
  tabIndex: number;
  serviceName?: string;
}

const setTab = createAction<
  { tabValue: IUserWeatherData; tabIndex: number },
  'userConfig/setWeather'
>('userConfig/setWeather');

const updateTab = createAction<ITabUpdateParams, 'userConfig/update'>('userConfig/update');

const addTab = createAction('userConfig/addTab');

const removeTab = createAction<number, 'userConfig/removeTab'>('userConfig/removeTab');

const refreshTab = createAction<ITabRefreshParams, 'userConfig/refreshTab'>(
  'userConfig/refreshTab',
);

export { setTab, updateTab, addTab, removeTab, refreshTab };
