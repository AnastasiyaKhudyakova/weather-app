import { IDailyWeather, IHourlyWeather, IWeatherForecast } from '../api/types';
import { mockForecastResponse, mockHourlyResponse, mockResponse } from '../services/mock/mock';

export interface IWeatherData {
  current: IDailyWeather;
  forecast: IWeatherForecast[];
  hourly: IHourlyWeather[];
}

export interface IWeatherState {
  weatherData: IWeatherData;
  isFetching: boolean;
  lastFetched: number;
  error: Error | null;
}

export const initWeatherState: IWeatherState = {
  weatherData: {
    current: mockResponse,
    forecast: mockForecastResponse,
    hourly: mockHourlyResponse,
  },
  isFetching: true,
  lastFetched: 0,
  error: null,
};
