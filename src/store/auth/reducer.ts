import {
  AnyAction,
  createReducer,
  createSlice,
  PayloadAction,
  SerializedError,
} from '@reduxjs/toolkit';
import { autologin, logout } from './types';

export interface IAuthState {
  user: IAuthUser | null;
  isLoading: boolean;
  error: Error | null;
}

export interface IAuthUser {
  uid: string;
  email: string;
  idToken: string;
}

const initialState: IAuthState = {
  user: null,
  isLoading: false,
  error: null,
};

const authReducers = createReducer(initialState, (builder) => {
  builder.addCase(logout, (state, action) => {
    state.user = null;
    state.isLoading = false;
    state.error = null;
  });
  builder.addCase(autologin, (state, action) => {
    state.user = action.payload;
  });
});

const isLoginSuccessAction = (action: AnyAction): action is PayloadAction<IAuthUser> => {
  return action.type.endsWith('login/fulfilled');
};

const isLoginFailedAction = (
  action: AnyAction,
): action is PayloadAction<undefined, string, null, SerializedError> => {
  return action.type.endsWith('login/rejected');
};

const isLoginPendingAction = (action: AnyAction): action is PayloadAction<undefined> => {
  return action.type.endsWith('login/pending');
};

const authSlice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    logout: authReducers,
    autologin: authReducers,
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(isLoginPendingAction, (state, action) => {
        state.user = null;
        state.isLoading = true;
        state.error = null;
      })
      .addMatcher(isLoginSuccessAction, (state, action) => {
        state.user = action.payload;
        state.isLoading = false;
        state.error = null;
      })
      .addMatcher(isLoginFailedAction, (state, action) => {
        state.user = null;
        state.isLoading = false;
        state.error = action.error as Error;
      });
  },
});

export { authSlice };
