import { createAction } from '@reduxjs/toolkit';
import { IAuthState, IAuthUser } from './reducer';
import { IUserConfig, IUserConfigTabVal } from '../../services/auth';

export const logout = createAction<undefined, 'auth/logout'>('auth/logout');
export const autologin = createAction<IAuthUser, 'auth/autologin'>('auth/autologin');
