import { createAsyncThunk } from '@reduxjs/toolkit';
import { IUserAuthData, signIn } from '../../auth/auth';
import { IAuthResponseData } from '../../auth/responseType';
import { getUserDatafromJWToken, removeUserCookies, setUserCookies } from '../../services/auth';
import { RootState } from '../rootReducer';
import { AppDispatch } from '../store';
import { IAuthUser } from './reducer';
import { logout, autologin } from './types';
import { fetchUserConfig } from '../userConfig/actions';

const logUserIn = createAsyncThunk<
  IAuthUser,
  IUserAuthData,
  {
    state: RootState;
  }
>('auth/login', async (authData: IUserAuthData, thunkAPI) => {
  const authResponseData: IAuthResponseData = await signIn(authData);
  const { localId: uid, idToken, email } = authResponseData;
  const authUser = { uid, email, idToken };

  thunkAPI.dispatch(fetchUserConfig({ uid, idToken }));
  setUserCookies(idToken);

  return authUser;
});

// replace any with whaaaaat?
const getAuthUserOnLoad = () => (dispatch: any): void => {
  const userData: IAuthUser | null = getUserDatafromJWToken();

  if (userData) {
    dispatch(autologin(userData));

    dispatch(fetchUserConfig(userData));
  } else {
    dispatch(logout());
  }
};

const logUserOut = () => (dispatch: AppDispatch) => {
  removeUserCookies();
  dispatch(logout());
};

export { logUserIn, getAuthUserOnLoad, logUserOut };
