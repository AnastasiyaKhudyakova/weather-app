import { RootState } from '../rootReducer';

const selectAuthState = (state: RootState) => state.auth;

export { selectAuthState };
