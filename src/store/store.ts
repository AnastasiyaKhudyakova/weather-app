import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const loggerMiddleware = createLogger();

const middleware = [thunkMiddleware];

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));

export type AppDispatch = typeof store.dispatch;

export { store };
