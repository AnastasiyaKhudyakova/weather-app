import React, { useState, useCallback } from 'react';
import { AutoComplete, Spin } from 'antd';
import { ICitiesDBCityData, ICitiesDBResponse } from '../../api/cititesDB/responseType';
import _ from 'lodash';
import { getCitiesByQuery } from '../../api/cititesDB';
import { Form, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import './CitySearch.less';

export interface ICityData {
  name: string;
  lon: number;
  lat: number;
}

const { Option } = AutoComplete;
const MIN_QUERY_SEARCH_LEN = 3;

const cyrillicPattern = /^[\u0400-\u04FF]+$/;
const getInputLngCode = (value: string): string => {
  return cyrillicPattern.test(value.replace(/\s+|-+/g, '')) ? 'ru' : 'en';
};

const CitySearch: React.FC<{ changePosition: (data: ICityData) => void }> = (props) => {
  const [query, setQuery] = useState<ICityData[]>([]);
  const [isLoading, setLoadinfStatus] = useState<boolean>(false);
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const delayedQuery = useCallback(
    _.debounce((value: string) => {
      setLoadinfStatus(true);
      getCitiesByQuery(value, getInputLngCode(value))
        .then((response: ICitiesDBResponse) => {
          const cities = response.data.map((city: ICitiesDBCityData) => {
            const { id, name, longitude, latitude } = city;
            const cityData: ICityData = { name, lon: longitude, lat: latitude };

            return cityData;
          });

          const filtered = cities.filter((cityData: ICityData) =>
            cityData.name.toLowerCase().includes(value.toLowerCase()),
          );

          setQuery(filtered);
        })
        .catch((error) => {
          setQuery([]);
        })
        .finally(() => setLoadinfStatus(false));
    }, 1000),
    [],
  );

  const handleSearch = (value: string) => {
    if (value.trim().length <= MIN_QUERY_SEARCH_LEN) {
      setQuery([]);
    } else {
      delayedQuery(value.trim());
    }
  };

  const handleSubmit = (values: { [key: string]: string }): void => {
    const cityData = query.find((city: ICityData) => city.name === values.city);
    const updatedPosition = cityData
      ? { lon: cityData.lon, lat: cityData.lat, name: cityData.name }
      : { lon: 0, lat: 0, name: 'Unknown Location' };

    props.changePosition(updatedPosition);
  };

  return (
    <div className='city_search_wrapper'>
      <Form className='city_search_form' form={form} name='basic' onFinish={handleSubmit}>
        <Form.Item name='city'>
          <AutoComplete
            className='city_search_form_autocomplete'
            onSearch={handleSearch}
            placeholder={t('search placeholder')}
          >
            {query.map((city, index) => (
              <Option key={city.name + '' + index} value={city.name}>
                {city.name}
              </Option>
            ))}
          </AutoComplete>
        </Form.Item>
        <Form.Item>
          <Button type='primary' htmlType='submit' disabled={!query.length}>
            {!isLoading ? t('change') : <Spin />}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CitySearch;
