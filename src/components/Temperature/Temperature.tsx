import React from 'react';
import './Temperature.less';

const TemperaturePane: React.FC<{ temperature: number }> = ({ temperature }) => {
  return (
    <h3 className='weather_card_temp'>
      {temperature}
      {'\u2103'}
    </h3>
  );
};

export default TemperaturePane;
