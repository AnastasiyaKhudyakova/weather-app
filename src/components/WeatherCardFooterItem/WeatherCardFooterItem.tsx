import React from 'react';
import { FooterIconMap } from './FooterIcons/FooterIconMap';
import { useTranslation } from 'react-i18next';
import './WeatherCardFooterItem.less';

export interface IWeatherCardFooterItemProps {
  name: string;
  value: string | number;
}

const MeasureUnitsMap: { [key: string]: string } = {
  feelsLike: '\u2103',
  pressure: 'hPa',
  humidity: '\u0025',
};

const WeatherCardFooterItem: React.FC<IWeatherCardFooterItemProps> = ({ name, value }) => {
  const { t } = useTranslation();
  const footerItem = (
    <li className='weather_card_footer_item'>
      <div className='weather_card_footer_item_fact'>
        <span className='weather_card_footer_item_fact_icon'>{FooterIconMap[name]}</span>
        <span className='weather_card_footer_item_fact_value'>
          {value} {t(MeasureUnitsMap[name])}
        </span>
      </div>
      <div className='weather_card_footer_item_name'>
        <span>{t(name)}</span>
      </div>
    </li>
  );

  return footerItem;
};

export default WeatherCardFooterItem;
