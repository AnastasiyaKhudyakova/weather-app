import React from 'react';
import { ReactComponent as Barometer } from './wi-barometer.svg';
import { ReactComponent as Thermometer } from './wi-thermometer.svg';
import { ReactComponent as Humidity } from './wi-humidity.svg';

export const FooterIconMap: { [key: string]: React.ReactElement } = {
  feelsLike: <Thermometer />,
  pressure: <Barometer />,
  humidity: <Humidity />,
};
