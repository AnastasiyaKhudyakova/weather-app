import React from 'react';
import './DatePane.less';

const DatePane: React.FC<{ timestamp?: number }> = ({ timestamp }) => {
  const lng = localStorage.getItem('i18nextLng') || 'en-US';
  const dayOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  const date = (timestamp ? new Date(timestamp) : new Date()).toLocaleDateString(lng, dayOptions);

  return <p className='date_pane'>{date}</p>;
};

export default DatePane;
