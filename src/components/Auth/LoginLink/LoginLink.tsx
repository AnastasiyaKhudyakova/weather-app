import React from 'react';
import { RootState } from '../../../store/rootReducer';
import { IAuthState } from '../../../store/auth/reducer';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Button } from 'antd';
import { logUserOut } from '../../../store/auth/actions';
import { selectAuthState } from '../../../store/auth/selectors';
import './LoginLink.less';

const LoginLink: React.FC = (): React.ReactElement => {
  const { t } = useTranslation();
  const { user } = useSelector<RootState, IAuthState>(selectAuthState);
  const dispatch = useDispatch();

  const handleClick = () => dispatch(logUserOut());

  return (
    <>
      {!user && (
        <li className='login_link_login'>
          <Link to={{ pathname: '/login' }}>{t('login')}</Link>
        </li>
      )}
      {user && (
        <li className='login_link_logout'>
          <Button type='link' onClick={handleClick}>
            {t('logout')}
          </Button>
        </li>
      )}
    </>
  );
};

export default LoginLink;
