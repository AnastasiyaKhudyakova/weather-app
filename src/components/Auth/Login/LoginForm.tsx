import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import './LoginForm.less';
import { IUserCredentials, IUserAuthData } from '../../../auth/auth';
import { Store } from 'antd/lib/form/interface';
import { logUserIn } from '../../../store/auth/actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store/rootReducer';
import { IAuthState } from '../../../store/auth/reducer';
import { selectAuthState } from '../../../store/auth/selectors';

const LoginForm: React.FC = () => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const { isLoading, error, user } = useSelector<RootState, IAuthState>(selectAuthState);
  const dispatch = useDispatch();
  const [isSignUp, setIsSignUp] = useState<boolean>(false);

  const onFinish = (values: Store) => {
    const authData: IUserAuthData = {
      ...(values as IUserCredentials),
      isSignUp,
    };

    dispatch(logUserIn(authData));
  };

  return (
    <>
      <h3 className='main__subtitle'>{isSignUp ? t('signup') : t('login')}</h3>
      <div className='login_form_toggle'>
        <Button type='ghost' htmlType='button' onClick={() => setIsSignUp(true)}>
          Sing up
        </Button>
        <Button type='ghost' htmlType='button' onClick={() => setIsSignUp(false)}>
          Sign in
        </Button>
      </div>
      {error && <p className='login_form_error'>{error.message}</p>}
      <Form className='login_form' form={form} name='basic' onFinish={onFinish}>
        <Form.Item
          className='login_form_item'
          label={t('email')}
          name='email'
          rules={[
            { required: true, message: t('email required') },
            { type: 'email', message: t('email invalid') },
          ]}
        >
          <Input className='login_form_item_input' />
        </Form.Item>

        <Form.Item
          className='login_form_item'
          label={t('password')}
          name='password'
          rules={[{ required: true, message: t('password required') }]}
        >
          <Input type='password' className='login_form_item_input' />
        </Form.Item>

        <Form.Item>
          <Button type='primary' htmlType='submit' disabled={isLoading}>
            {isSignUp ? t('signup') : t('login')}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default LoginForm;
