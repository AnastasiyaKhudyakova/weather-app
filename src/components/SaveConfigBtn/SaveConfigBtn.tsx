import { SaveOutlined } from '@ant-design/icons';
import { Badge, Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';
import { selectUserConfigStatus } from '../../store/userConfig/selectors';
import './SaveConfigBtn.less';
import { saveUserConfig } from '../../store/userConfig/actions';

const SaveConfigBtn: React.FC = (): React.ReactElement => {
  const isSaved = useSelector<RootState, boolean>(selectUserConfigStatus);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const saveConfig = () => dispatch(saveUserConfig());

  return (
    <Button className='btn_save_config' type='link' onClick={saveConfig}>
      <SaveOutlined />
      {t('save config')}
      {!isSaved && <Badge dot className='btn_save_config_badge' />}
    </Button>
  );
};

export default SaveConfigBtn;
