import React from 'react';
import { Spin } from 'antd';
import './Spinner.less';

const Spinner: React.FC = (): React.ReactElement => {
  return (
    <div className='spinner_wrapper'>
      <Spin className='spinner' size='large' />
    </div>
  );
};

export default Spinner;
