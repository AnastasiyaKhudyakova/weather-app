import React from 'react';
import WeatherIcon from '../Icon/WeatherIcon';
import { useTranslation } from 'react-i18next';
import './WeatherCardError.less';

const WeatherCardError: React.FC = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <div className='weather_card_error_wrapper'>
      <p className='weather_card_error_title'>{t('fetch error')}</p>
      <WeatherIcon condition='cloudy' />
      <p className='weather_card_error_message'>{t('service unavailable')}</p>
    </div>
  );
};

export default WeatherCardError;
