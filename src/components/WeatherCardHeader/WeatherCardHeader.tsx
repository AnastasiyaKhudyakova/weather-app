import React from 'react';
import WeatherServiceLogo from '../WeatherServiceLogo/WeatherServiceLogo';
import TimePane from '../Time/TimePane';
import DatePane from '../Date/DatePane';
import './WeatherCardHeader.less';

const WeatherCardHeader: React.FC<{ serviceName: string }> = ({
  serviceName,
}): React.ReactElement => {
  return (
    <>
      <WeatherServiceLogo serviceName={serviceName} />
      <TimePane />
      <DatePane />
    </>
  );
};

export default WeatherCardHeader;
