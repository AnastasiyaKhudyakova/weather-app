import React from 'react';
import './TimePane.less';

const TimePane: React.FC<{ timestamp?: number }> = ({ timestamp }): React.ReactElement => {
  const lng = localStorage.getItem('i18nextLng') || 'en-US';

  const time = (timestamp ? new Date(timestamp) : new Date()).toLocaleTimeString(lng, {
    hour: 'numeric',
    minute: 'numeric',
  });

  return <p className='time_pane'>{time}</p>;
};

export default TimePane;
