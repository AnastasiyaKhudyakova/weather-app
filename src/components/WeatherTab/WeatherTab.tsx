import { ReloadOutlined } from '@ant-design/icons';
import React from 'react';
import { useDispatch } from 'react-redux';
import { ICoords } from '../../pages/Main/Main';
import { initWeatherState } from '../../store/types';
import { updateWeatherTab } from '../../store/userConfig/actions';
import { IUserTabConfigs, IUserWeatherData } from '../../store/userConfig/reducer';
import { refreshTab } from '../../store/userConfig/types';
import CheckBoxes from '../CheckBoxes/CheckBoxes';
import CitySearch, { ICityData } from '../Search/CitySearch';
import WeatherCard from '../WeatherCard/WeatherCard';
import './WeatherTab.less';
import WeatherServiceModal from '../Modal/WeatherServiceModal';
import { Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';

export interface IWeatherTabProps {
  tabIndex: number;
  tabValue: IUserTabConfigs;
}

const WeatherTab: React.FC<IWeatherTabProps> = (props) => {
  const { tabValue, tabIndex } = props;
  const displayedServices = Object.keys(tabValue.weather);
  // const displayedServices = ['yandex'];
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { lon, lat } = tabValue;

  const changePosition = (data: ICityData): void => {
    const tabWeather: IUserWeatherData = {};

    Object.keys(tabValue.weather).forEach((weatherService) => {
      tabWeather[weatherService] = { ...initWeatherState };
    });

    const updatedTabVal = { weather: tabWeather, lon: data.lon, lat: data.lat, city: data.name };

    dispatch(updateWeatherTab({ tabValue: updatedTabVal, tabIndex }));
  };

  const updateDisplayedServices = (serviceNames: string[]): void => {
    const updatedTabs = {} as IUserWeatherData;

    serviceNames.forEach((name) => {
      if (tabValue.weather[name]) {
        updatedTabs[name] = { ...tabValue.weather[name] };
      } else {
        updatedTabs[name] = { ...initWeatherState };
      }
    });

    const updatedConfig: IUserTabConfigs = { ...tabValue, weather: updatedTabs };

    dispatch(updateWeatherTab({ tabValue: updatedConfig, tabIndex }));
  };

  const weatherCards = displayedServices.map((serviceName: string) => (
    <WeatherCard
      key={serviceName}
      serviceName={serviceName}
      coords={{ lon, lat } as ICoords}
      tabIndex={tabIndex}
      tabValue={tabValue}
    />
  ));

  return (
    <>
      <div className='weather-tab_utils_pane'>
        <Tooltip placement='topLeft' title={t('reloadAll')}>
          <ReloadOutlined
            className='weather-tab_utils_icon_reload'
            onClick={() => dispatch(refreshTab({ tabIndex }))}
          />
        </Tooltip>

        <WeatherServiceModal
          tabConfig={tabValue}
          updateDisplayedServices={updateDisplayedServices}
        />
      </div>

      <CitySearch changePosition={changePosition} />
      <div className='main__weather_card_container'>{weatherCards}</div>
    </>
  );
};

export default WeatherTab;
