import React from 'react';
import { WeatherIconsMap } from './iconMap';
import { WeatherIconsDay } from './weatherIconsDay';
import { WeatherIconsNight } from './weatherIconsNight';
import { ReactComponent as Unknown } from './weatherIcons/unknown.svg';

interface IWeatherIconProps {
  condition: string;
  sunset?: number;
  sunrise?: number;
}

const isDayTime = (sunset: number = 0, sunrise: number = 0): boolean => {
  const currentTime = new Date().getTime();

  return currentTime >= sunrise && currentTime <= sunset;
};

const WeatherIcon: React.FC<IWeatherIconProps> = ({
  condition,
  sunset,
  sunrise,
}): React.ReactElement => {
  const iconAlias = WeatherIconsMap[condition] || null;

  let weatherIcon: React.ReactNode;

  if (iconAlias) {
    if (sunset && sunrise) {
      weatherIcon = isDayTime(sunset, sunrise)
        ? WeatherIconsDay[iconAlias]
        : WeatherIconsNight[iconAlias];
    } else {
      weatherIcon = WeatherIconsDay[iconAlias];
    }
  }

  return <div className='weather_card_icon'>{weatherIcon || <Unknown />}</div>;
};

export default WeatherIcon;
