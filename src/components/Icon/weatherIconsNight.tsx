import React, { ReactNode } from 'react';
import { ReactComponent as Chanceflurries } from './weatherIcons/nt_chanceflurries.svg';
import { ReactComponent as Chancerain } from './weatherIcons/nt_chancerain.svg';
import { ReactComponent as Rain } from './weatherIcons/nt_rain.svg';
import { ReactComponent as Chancesleet } from './weatherIcons/nt_chancesleet.svg';
import { ReactComponent as Chancetstorms } from './weatherIcons/nt_chancetstorms.svg';
import { ReactComponent as Clear } from './weatherIcons/nt_clear.svg';
import { ReactComponent as Cloudy } from './weatherIcons/nt_cloudy.svg';
import { ReactComponent as Flurries } from './weatherIcons/nt_flurries.svg';
import { ReactComponent as Fog } from './weatherIcons/nt_fog.svg';
import { ReactComponent as Hazy } from './weatherIcons/nt_hazy.svg';
import { ReactComponent as Mostlycloudy } from './weatherIcons/nt_mostlycloudy.svg';
import { ReactComponent as Mostlysunny } from './weatherIcons/nt_mostlysunny.svg';
import { ReactComponent as Snow } from './weatherIcons/nt_snow.svg';
import { ReactComponent as Sleet } from './weatherIcons/nt_sleet.svg';
import { ReactComponent as Tstorm } from './weatherIcons/nt_tstorms.svg';

export const WeatherIconsNight: { [key: string]: ReactNode } = {
  chancesflurries: <Chanceflurries />,
  chancerain: <Chancerain />,
  rain: <Rain />,
  chancesleet: <Chancesleet />,
  chancetstorms: <Chancetstorms />,
  clear: <Clear />,
  cloudy: <Cloudy />,
  flurries: <Flurries />,
  fog: <Fog />,
  hazy: <Hazy />,
  mostlycloudy: <Mostlycloudy />,
  mostlysunny: <Mostlysunny />,
  snow: <Snow />,
  sleet: <Sleet />,
  tstorm: <Tstorm />,
};
