import React, { ReactNode } from 'react';
import { ReactComponent as Chanceflurries } from './weatherIcons/chanceflurries.svg';
import { ReactComponent as Chancerain } from './weatherIcons/chancerain.svg';
import { ReactComponent as Rain } from './weatherIcons/rain.svg';
import { ReactComponent as Chancesleet } from './weatherIcons/chancesleet.svg';
import { ReactComponent as Chancetstorms } from './weatherIcons/chancetstorms.svg';
import { ReactComponent as Clear } from './weatherIcons/clear.svg';
import { ReactComponent as Cloudy } from './weatherIcons/cloudy.svg';
import { ReactComponent as Flurries } from './weatherIcons/flurries.svg';
import { ReactComponent as Fog } from './weatherIcons/fog.svg';
import { ReactComponent as Hazy } from './weatherIcons/hazy.svg';
import { ReactComponent as Mostlycloudy } from './weatherIcons/mostlycloudy.svg';
import { ReactComponent as Mostlysunny } from './weatherIcons/mostlysunny.svg';
import { ReactComponent as Snow } from './weatherIcons/snow.svg';
import { ReactComponent as Sleet } from './weatherIcons/sleet.svg';
import { ReactComponent as Tstorm } from './weatherIcons/tstorms.svg';

export const WeatherIconsDay: { [key: string]: ReactNode } = {
  chancesflurries: <Chanceflurries />,
  chancerain: <Chancerain />,
  rain: <Rain />,
  chancesleet: <Chancesleet />,
  chancetstorms: <Chancetstorms />,
  clear: <Clear />,
  cloudy: <Cloudy />,
  flurries: <Flurries />,
  fog: <Fog />,
  hazy: <Hazy />,
  mostlycloudy: <Mostlycloudy />,
  mostlysunny: <Mostlysunny />,
  snow: <Snow />,
  sleet: <Sleet />,
  tstorm: <Tstorm />,
};
