import { SaveOutlined } from '@ant-design/icons';
import React from 'react';
import { Link, Route } from 'react-router-dom';
import LoginLink from '../Auth/LoginLink/LoginLink';
import './Navbar.less';
import { Button } from 'antd';
import SaveConfigBtn from '../SaveConfigBtn/SaveConfigBtn';

export interface INavbarLink {
  name: string;
  link: string;
}

const Navbar: React.FC = (): React.ReactElement => {
  return (
    <Route>
      <header className='header_container'>
        <div className='header_navbar'>
          <div className='header_title_home_link'>
            <Link to={{ pathname: '/' }}>Weather App</Link>
          </div>
          <span className='spacer'></span>
          <SaveConfigBtn />
          <LoginLink />
        </div>
      </header>
    </Route>
  );
};

export default Navbar;
