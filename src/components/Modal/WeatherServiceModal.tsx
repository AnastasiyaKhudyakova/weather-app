import { PlusCircleFilled } from '@ant-design/icons';
import { Modal, Tooltip } from 'antd';
import React, { useState } from 'react';
import { IUserTabConfigs } from '../../store/userConfig/reducer';
import CheckBoxes from '../CheckBoxes/CheckBoxes';
import './WeatherServiceModal.less';
import { useTranslation } from 'react-i18next';

export interface IWeatherServiceModalProps {
  tabConfig: IUserTabConfigs;
  updateDisplayedServices: (serviceNames: string[]) => void;
}

const WeatherServiceModal: React.FC<IWeatherServiceModalProps> = (props) => {
  const { tabConfig, updateDisplayedServices } = props;
  const [isModalVisible, setModalVisible] = useState<boolean>(false);
  const [pickedServices, setPickedServices] = useState<string[]>(Object.keys(tabConfig.weather));
  const { t } = useTranslation();

  const setDisplayedServices = () => {
    updateDisplayedServices(pickedServices);
    setModalVisible(false);
  };

  const handleChange = (serviceNames: string[]) => setPickedServices(serviceNames);

  return (
    <>
      <Tooltip placement='topLeft' title={t('addCard')}>
        <PlusCircleFilled
          className='weather-service-modal_btn_toggle'
          onClick={() => setModalVisible(true)}
        />
      </Tooltip>
      <Modal
        title={<h5 className='main__subtitle'>{t('pick services')}</h5>}
        centered
        visible={isModalVisible}
        onCancel={() => setModalVisible(false)}
        onOk={() => setDisplayedServices()}
      >
        <CheckBoxes serviceNames={pickedServices} handleChange={handleChange} />
      </Modal>
    </>
  );
};

export default WeatherServiceModal;
