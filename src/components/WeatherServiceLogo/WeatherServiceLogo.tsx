import React from 'react';
import { useTranslation } from 'react-i18next';
import './WeatherServiceLogo.less';

const WeatherServiceLogo: React.FC<{ serviceName: string }> = ({ serviceName }) => {
  const { t } = useTranslation();

  return (
    <div className='weather-service_logo'>
      <img src={`/images/${serviceName}.png`} alt={t('serviceLogo')} />
    </div>
  );
};

export default WeatherServiceLogo;
