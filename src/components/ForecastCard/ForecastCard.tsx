import React from 'react';
import { IWeatherForecast } from '../../api/types';
import { Card } from 'antd';
import TemparaturePane from '../Temperature/Temperature';
import WeatherIcon from '../Icon/WeatherIcon';
import DatePane from '../Date/DatePane';
import { useTranslation } from 'react-i18next';
import './ForecastCard.less';

const ForecastCard: React.FC<{ dailyWeather: IWeatherForecast }> = ({ dailyWeather }) => {
  const { t } = useTranslation();
  const { date, tempMin, tempMax, condition } = dailyWeather;

  return (
    <Card className='forecast_card'>
      <DatePane timestamp={date} />
      <div className='forecast_card_fact'>
        <WeatherIcon condition={condition} />
        <div>
          <TemparaturePane temperature={tempMin} />
          <TemparaturePane temperature={tempMax} />
        </div>
      </div>
      <p className='forecast_card_condition'>{t(dailyWeather.condition)}</p>
    </Card>
  );
};

export default ForecastCard;
