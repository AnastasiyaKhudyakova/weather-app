import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ICoords } from '../../pages/Main/Main';
import { getTabWeather, IWeatherRequestParams } from '../../store/userConfig/actions';
// import { selectWeatherByIndex } from '../../store/tabs/selectors';
import CurrentWeatherCard from '../CurrentWeather/CurrentWeatherCard';
import IconLoader from '../IconLoader/IconLoader';
import { Spin, Button } from 'antd';
import Spinner from '../Spinner/Spinner';
import { selectUserConfigTab } from '../../store/userConfig/selectors';
import { ReloadOutlined } from '@ant-design/icons';
import { IUserTabConfigs } from '../../store/userConfig/reducer';
import { refreshTab } from '../../store/userConfig/types';

interface IWeatherCardProps {
  coords: ICoords;
  serviceName: string;
  tabIndex: number;
  tabValue: IUserTabConfigs;
}

const WeatherCard: React.FC<IWeatherCardProps> = (props): React.ReactElement | null => {
  const { coords, serviceName, tabIndex } = props;
  const config = useSelector(selectUserConfigTab(tabIndex));

  const dispatch = useDispatch();

  const fetchWeather = () => {
    const reqParams: IWeatherRequestParams = {
      apiName: serviceName,
      tabIndex,
      coords,
    };

    dispatch(getTabWeather(reqParams));
  };

  useEffect(() => {
    if (config[serviceName].isFetching) {
      fetchWeather();
    }
  }, [config[serviceName].isFetching]);

  const card = (
    <CurrentWeatherCard serviceName={serviceName} tabIndex={tabIndex}>
      <ReloadOutlined onClick={() => dispatch(refreshTab({ tabIndex, serviceName }))} />
    </CurrentWeatherCard>
  );

  return config?.[serviceName] ? card : <Spinner />;
};

export default WeatherCard;
