import React from 'react';
import WeatherCardFooterItem from '../WeatherCardFooterItem/WeatherCardFooterItem';
import './WeatherCardFooter.less';

const WeatherCardFooter: React.FC<{ [key: string]: number | string }> = (
  props,
): React.ReactElement => {
  const footerListItems = Object.keys(props).map((key: string) => {
    return <WeatherCardFooterItem key={`footer-item-${key}`} name={key} value={props[key]} />;
  });

  return <ul className='weather_card_footer'>{footerListItems}</ul>;
};

export default WeatherCardFooter;
