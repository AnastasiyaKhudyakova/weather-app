import { ReloadOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';
import { IWeatherState } from '../../store/types';
import { selectWeatherByServiceName } from '../../store/userConfig/selectors';
import DatePane from '../Date/DatePane';
import Spinner from '../Spinner/Spinner';
import TimePane from '../Time/TimePane';
import WeatherCardContent from '../WeatherCardContent/WeatherCardContent';
import WeatherCardError from '../WeatherCardError/WeatherCardError';
import WeatherServiceLogo from '../WeatherServiceLogo/WeatherServiceLogo';
import './CurrentWeatherCard.less';
import WeatherCardHeader from '../WeatherCardHeader/WeatherCardHeader';

export interface ICurrentWeatherCardProps {
  serviceName: string;
  tabIndex: number;
  children: React.ReactElement;
}

const CurrentWeatherCard: React.FC<ICurrentWeatherCardProps> = (props): React.ReactElement => {
  const { tabIndex, serviceName, children } = props;

  const weatherState = useSelector<RootState, IWeatherState>(
    selectWeatherByServiceName(tabIndex, serviceName),
  );

  const weatherCardHeader = useMemo(() => <WeatherCardHeader serviceName={serviceName} />, [
    weatherState.weatherData,
  ]);

  const currentWeatherCard = !weatherState.isFetching ? (
    <Card className='main_weather_card'>
      {children}
      {weatherCardHeader}
      {weatherState?.weatherData && !weatherState?.error && (
        <WeatherCardContent weatherData={weatherState.weatherData.current} />
      )}
      {weatherState?.error && <WeatherCardError />}
    </Card>
  ) : (
    <Spinner />
  );

  return currentWeatherCard;
};

export default CurrentWeatherCard;
