import React from 'react';
import { IHourlyWeather } from '../../api/types';
import {
  AreaChart,
  XAxis,
  YAxis,
  Tooltip,
  Area,
  ResponsiveContainer,
  TooltipProps,
} from 'recharts';
import WeatherIcon from '../Icon/WeatherIcon';
import TemperaturePane from '../Temperature/Temperature';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import './ForecastChart.less';

interface IChartData {
  date: string;
  temp: number;
  condition: string;
}

const convertTimestampToShortDate = (timestamp: number): string => {
  const lng = localStorage.getItem('i18nextLng') || 'en-US';
  const options = { hour: 'numeric' };
  const shortDate = new Date(timestamp).toLocaleString(lng, options);

  return `${shortDate}:00`;
};

const getTooltipContent = (
  label: string | number | undefined,
  data: IChartData[] = [],
  translate: TFunction,
) => {
  const found = data.find(({ date }: IChartData) => date === label);

  return found ? (
    <div className='forecast_chart_tooltip'>
      <p className='forecast_chart_tooltip_date'>{found.date}</p>
      <div className='forecast_chart_tooltip_fact'>
        <TemperaturePane temperature={found.temp} />
        <WeatherIcon condition={found.condition} />
      </div>
      <p className='forecast_chart_tooltip_condition'>{translate(found.condition)}</p>
    </div>
  ) : null;
};

const ForecastChart: React.FC<{ hourlyForecast: IHourlyWeather[] }> = ({
  hourlyForecast,
}): React.ReactElement | null => {
  const { t } = useTranslation();

  const hourlyWeatherData: IChartData[] = hourlyForecast.map((hourlyWeather: IHourlyWeather) => {
    return {
      ...hourlyWeather,
      date: convertTimestampToShortDate(hourlyWeather.date),
    };
  });

  return (
    <ResponsiveContainer width='100%' height={200}>
      <AreaChart className='forecast__area_chart' data={hourlyWeatherData}>
        <defs>
          <linearGradient id='colorUv' x1='0' y1='0' x2='0' y2='1'>
            <stop offset='5%' stopColor='#8884d8' stopOpacity={0.8} />
            <stop offset='95%' stopColor='#8884d8' stopOpacity={0} />
          </linearGradient>
          <linearGradient id='colorPv' x1='0' y1='0' x2='0' y2='1'>
            <stop offset='5%' stopColor='#82ca9d' stopOpacity={0.8} />
            <stop offset='95%' stopColor='#82ca9d' stopOpacity={0} />
          </linearGradient>
        </defs>
        <XAxis dataKey='date' />
        <YAxis dataKey='temp' />
        <Tooltip
          content={(props: TooltipProps) => {
            return <div>{getTooltipContent(props.label, hourlyWeatherData, t)}</div>;
          }}
        />
        <Area
          type='monotone'
          dataKey='temp'
          stroke='#8884d8'
          fillOpacity={1}
          fill='url(#colorUv)'
        />
        //{' '}
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default ForecastChart;
