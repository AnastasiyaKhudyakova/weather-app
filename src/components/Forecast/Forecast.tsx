import React from 'react';
import { IWeatherForecast } from '../../api/types';
import ForecastCard from '../ForecastCard/ForecastCard';
import { Card } from 'antd';
import './Forecast.less';

const Forecast: React.FC<{ forecastWeather: IWeatherForecast[] }> = ({
  forecastWeather,
}): React.ReactElement => {
  const forecastData = forecastWeather.map((dailyWeather: IWeatherForecast, index: number) => {
    return (
      <Card.Grid key={`dailyWeather-${index}`}>
        <ForecastCard dailyWeather={dailyWeather} />
      </Card.Grid>
    );
  });

  return <Card className='forecast_container'>{forecastData}</Card>;
};

export default Forecast;
