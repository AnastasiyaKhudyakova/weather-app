import { Checkbox } from 'antd';
import { CheckboxValueType, CheckboxGroupState } from 'antd/lib/checkbox/Group';
import React, { useState } from 'react';
import { WEATHER_APIS } from '../../App';
import { IUserTabConfigs } from '../../store/userConfig/reducer';
import './Checkboxes.less';
import WeatherServiceLogo from '../WeatherServiceLogo/WeatherServiceLogo';

export interface ICheckBoxValues {
  label: string;
  value: string;
}

export interface ICheckBoxesProps {
  // tabConfig: IUserTabConfigs;
  serviceNames: string[];
  handleChange: (serviceNames: string[]) => void;
}

const CheckBoxes: React.FC<ICheckBoxesProps> = (props): React.ReactElement => {
  const { serviceNames, handleChange } = props;
  const defaultCheckedList: CheckboxValueType[] = serviceNames;

  const onChange = (checkedList: CheckboxValueType[]) => handleChange(checkedList as string[]);

  const options = WEATHER_APIS.map((serviceName: string) => {
    return {
      label: <WeatherServiceLogo serviceName={serviceName} />,
      value: serviceName,
      disabled: defaultCheckedList.length < 2 && defaultCheckedList[0] === serviceName,
    };
  });

  return (
    <div className='checkbox_group_wrapper'>
      <Checkbox.Group options={options} defaultValue={defaultCheckedList} onChange={onChange} />
    </div>
  );
};

export default CheckBoxes;
