import React from 'react';
import { useTranslation } from 'react-i18next';
import { IDailyWeather } from '../../api/types';
import TemperaturePane from '../Temperature/Temperature';
import WeatherIcon from '../Icon/WeatherIcon';
import WeatherCardFooter from '../WeatherCardFooter/WeatherCardFooter';
import './WeatherCardContent.less';

const WeatherCardContent: React.FC<{ weatherData: IDailyWeather }> = ({ weatherData }) => {
  const { t } = useTranslation();
  const { temperature, condition, sunset, sunrise, feelsLike, humidity, pressure } = weatherData;

  return (
    <>
      <div className='weather_card_content'>
        <div className='weather_card_content_info'>
          <TemperaturePane temperature={temperature} />
          <WeatherIcon condition={condition} sunset={sunset} sunrise={sunrise} />
        </div>
        <p className='weather_card_content_condition'>{t(condition)}</p>
      </div>
      <div>
        <WeatherCardFooter feelsLike={feelsLike} humidity={humidity} pressure={pressure} />
      </div>
    </>
  );
};

export default WeatherCardContent;
