import React from 'react';
import './IconLoader.less';
import { useTranslation } from 'react-i18next';
import loader from './weather.svg';

const IconLoader: React.FC = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <div className='icon_loader_container'>
      <img className='icon_loader_icon' src={loader} alt={t('loader icon')} />
      <h6 className='icon_loader_message'>{t('loading')}</h6>
    </div>
  );
};

export default IconLoader;
