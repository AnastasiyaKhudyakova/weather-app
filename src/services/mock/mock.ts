import { IDailyWeather, IHourlyWeather, IWeatherForecast } from '../../api/types';
import { IUserConfig } from '../auth';

const date = new Date();

export const mockResponse: IDailyWeather = {
  temperature: 23,
  humidity: 66,
  pressure: 1000,
  feelsLike: 24,
  condition: 'clear',
  sunset: date.setHours(21),
  sunrise: date.setHours(6),
};

export const mockHourlyResponse: IHourlyWeather[] = [
  { date: 1594620000000, temp: 10, condition: 'clouds' },
  { date: 1594623600000, temp: 13, condition: 'clouds' },
  { date: 1594627200000, temp: 15, condition: 'rain' },
  { date: 1594630800000, temp: 16, condition: 'rain' },
  { date: 1594634400000, temp: 17, condition: 'clouds' },
  { date: 1594638000000, temp: 18, condition: 'clouds' },
  { date: 1594641600000, temp: 18, condition: 'rain' },
  { date: 1594645200000, temp: 19, condition: 'clouds' },
  { date: 1594648800000, temp: 19, condition: 'rain' },
  { date: 1594652400000, temp: 19, condition: 'clouds' },
  { date: 1594656000000, temp: 19, condition: 'clouds' },
  { date: 1594659600000, temp: 18, condition: 'clouds' },
  { date: 1594663200000, temp: 16, condition: 'clouds' },
  { date: 1594666800000, temp: 13, condition: 'clear' },
  { date: 1594670400000, temp: 13, condition: 'clear' },
  { date: 1594674000000, temp: 12, condition: 'clear' },
  { date: 1594677600000, temp: 11, condition: 'clear' },
  { date: 1594681200000, temp: 10, condition: 'clear' },
  { date: 1594684800000, temp: 10, condition: 'clear' },
  { date: 1594688400000, temp: 10, condition: 'clouds' },
  { date: 1594692000000, temp: 10, condition: 'clouds' },
  { date: 1594695600000, temp: 10, condition: 'clouds' },
  { date: 1594699200000, temp: 11, condition: 'clouds' },
  { date: 1594702800000, temp: 13, condition: 'clouds' },
];

export const mockForecastResponse: IWeatherForecast[] = [
  { date: 1594634400000, tempMax: 18, tempMin: 10, condition: 'rain' },
  { date: 1594720800000, tempMax: 20, tempMin: 9, condition: 'rain' },
  { date: 1594807200000, tempMax: 23, tempMin: 11, condition: 'clear' },
  { date: 1594893600000, tempMax: 24, tempMin: 12, condition: 'rain' },
  { date: 1594980000000, tempMax: 25, tempMin: 14, condition: 'rain' },
  { date: 1595066400000, tempMax: 23, tempMin: 12, condition: 'rain' },
  { date: 1595152800000, tempMax: 24, tempMin: 11, condition: 'clear' },
  { date: 1595239200000, tempMax: 24, tempMin: 12, condition: 'clear' },
];

export const MOCK_CONFIG: IUserConfig = {
  tab1: {
    lon: 23,
    lat: 18,
    city: 'London',
    services: ['weatherMap', 'weatherBit'],
  },
  tab2: {
    lon: 58,
    lat: 28,
    city: 'Some City',
    services: ['climacell', 'weatherMap', 'yandex'],
  },
  tab3: {
    lon: -84,
    lat: 23,
    city: 'Another City',
    services: ['weatherMap', 'weatherBit'],
  },
};
