import { IClimacellApiRes } from '../api/climacell/responseType';
import { IDailyWeather, IWeatherForecast, IHourlyWeather } from '../api/types';
import { ICoords } from '../pages/Main/Main';
import { IWeatherData } from '../store/types';
import { getWeather } from '../api/climacell';

const getCurrentWeatherData = (responseData: IClimacellApiRes[]): IDailyWeather => {
  const {
    temp,
    humidity,
    baro_pressure,
    feels_like,
    weather_code,
    sunset,
    sunrise,
  } = responseData[0];

  return {
    temperature: Math.ceil(temp.value),
    humidity: Math.ceil(humidity.value),
    pressure: Math.ceil(baro_pressure.value),
    feelsLike: Math.ceil(feels_like.value),
    condition: weather_code.value,
    sunset: new Date(sunset.value).getTime(),
    sunrise: new Date(sunrise.value).getTime(),
  };
};

const getForecastData = (dailyWeather: IClimacellApiRes[]): IWeatherForecast[] => {
  const forecatsByDay = dailyWeather.reduce(
    (accum: { [key: string]: IClimacellApiRes[] }, current: IClimacellApiRes) => {
      const key = getDateOnly(current.observation_time.value);

      if (!accum[key]) {
        accum[key] = [];
      }
      accum[key].push(current);

      return accum;
    },
    {},
  );

  const forecastData: IWeatherForecast[] = [];

  Object.keys(forecatsByDay).forEach((key: string) => {
    const { tempMax, tempMin } = getMinMaxDayTempature(forecatsByDay[key]);

    forecastData.push({
      date: new Date(key).getTime(),
      tempMax: Math.ceil(tempMax),
      tempMin: Math.floor(tempMin),
      condition: getAvarageWeatherCondition(forecatsByDay[key]),
    });
  });

  return forecastData;
};

const getHourlyWeatherData = (dailyWeather: IClimacellApiRes[]): IHourlyWeather[] => {
  return dailyWeather.slice(0, 24).map((currentDayHourlyForecast: IClimacellApiRes) => {
    const { observation_time, temp, weather_code } = currentDayHourlyForecast;
    const hourlyWeather: IHourlyWeather = {
      date: new Date(observation_time.value).getTime(),
      temp: Math.ceil(temp.value),
      condition: weather_code.value,
    };

    return hourlyWeather;
  });
};

const getDateOnly = (date: string): string => {
  const initialDate = new Date(date);

  return new Date(
    initialDate.getFullYear(),
    initialDate.getMonth(),
    initialDate.getDate(),
  ).toString();
};

const getMinMaxDayTempature = (
  weatherData: IClimacellApiRes[],
): { tempMax: number; tempMin: number } => {
  const tempratureValues = weatherData.map(
    (hourlyWeather: IClimacellApiRes) => hourlyWeather.temp.value,
  );

  return {
    tempMax: Math.max(...tempratureValues),
    tempMin: Math.min(...tempratureValues),
  };
};

const getAvarageWeatherCondition = (weatherData: IClimacellApiRes[]): string => {
  const conditionMap = weatherData.reduce(
    (accum: { [key: string]: number }, current: IClimacellApiRes) => {
      const weatherCondition = current.weather_code.value;

      accum[weatherCondition] = !accum[weatherCondition] ? 1 : (accum[weatherCondition] += 1);

      return accum;
    },
    {},
  );
  const avarageCondition: [string, number] = Object.entries(conditionMap).sort(
    (condition1: [string, number], condition2: [string, number]) => condition2[1] - condition1[1],
  )[0];

  return avarageCondition[0];
};

const getClimacellWeather = (coords: ICoords): Promise<IWeatherData> => {
  return getWeather(coords).then((response: IClimacellApiRes[]) => {
    const weatherData: IWeatherData = {
      current: getCurrentWeatherData(response),
      forecast: getForecastData(response),
      hourly: getHourlyWeatherData(response),
    };

    return weatherData;
  });
};

export { getCurrentWeatherData, getForecastData, getHourlyWeatherData, getClimacellWeather };
