import { IUserAuthData } from '../auth/auth';
import { IAuthUser } from '../store/auth/reducer';

export interface IUserConfig {
  [key: string]: any;
}

export interface IUserConfigTabVal {
  [key: string]: number | string | string[];
  lon: number | string;
  lat: number | string;
  city: string;
  services: string[];
}

export interface IParsedUserData {
  name: string;
  iss: string;
  aud: string;
  auth_time: number;
  user_id: string;
  sub: string;
  iat: number;
  exp: number;
  email: string;
  email_verified: boolean;
  firebase: {
    identities: { [key: string]: string | string[] };
    sign_in_provider: string;
  };
}

export interface IUserCredentials {
  user: IUserAuthData;
  isValid: boolean;
}

const getUserTokenCookies = (): string | undefined => {
  const userIdToken = document.cookie
    .split('; ')
    .find((row: string) => row.startsWith('weather-app-id-token'))
    ?.split('=')[1];

  return userIdToken;
};

const removeUserCookies = () => (document.cookie = 'weather-app-id-token=0');

const setUserCookies = (idToken: string) => (document.cookie = `weather-app-id-token=${idToken}`);

const getUserDatafromJWToken = (): IAuthUser | null => {
  const token = getUserTokenCookies();

  if (!token) {
    return null;
  }

  try {
    const parsedToken: IParsedUserData = JSON.parse(window.atob(token.split('.')[1]));

    if (parsedToken.exp * 1000 > new Date().getTime()) {
      const user: IAuthUser = {
        uid: parsedToken.user_id,
        email: parsedToken.email,
        idToken: token,
      };

      return user;
    }

    return null;
  } catch (error) {
    return null;
  }
};

export { getUserDatafromJWToken, getUserTokenCookies, removeUserCookies, setUserCookies };
