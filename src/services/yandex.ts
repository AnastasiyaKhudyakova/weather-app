import {
  IYandexApiResponseData,
  IYandexForecastsRes,
  IYandexHourForecastRes,
} from '../api/yandex/responseType';
import { IDailyWeather, IWeatherForecast, IHourlyWeather } from '../api/types';
import { parseDate } from './utils';
import { ICoords } from '../pages/Main/Main';
import { getWeather } from '../api/yandex';
import { IWeatherData } from '../store/types';

const getCurrentWeatherData = ({ fact, forecasts }: IYandexApiResponseData): IDailyWeather => {
  const { sunset, sunrise } = parseDate(
    new Date().getTime(),
    forecasts[0].sunset,
    forecasts[0].sunrise,
  );

  return {
    temperature: fact.temp,
    humidity: fact.humidity,
    pressure: Math.ceil(fact.pressure_pa),
    feelsLike: fact.feels_like,
    condition: fact.condition,
    sunset,
    sunrise,
  };
};

const getForecastData = (dailyWeather: IYandexForecastsRes[]): IWeatherForecast[] => {
  const forecast = dailyWeather.map((dailyData: IYandexForecastsRes) => {
    const { date_ts, parts } = dailyData;
    const dailyForecast: IWeatherForecast = {
      date: date_ts * 1000,
      tempMax: parts.night.temp_min,
      tempMin: parts.day.temp_max,
      condition: parts.day.condition,
    };

    return dailyForecast;
  });

  return forecast;
};

const getHourlyWeatherData = (hourlyWeather: IYandexForecastsRes[]): IHourlyWeather[] => {
  const now = new Date().getTime();

  return hourlyWeather[0].hours
    .concat(hourlyWeather[1].hours)
    .filter((hourlyForecast: IYandexHourForecastRes) => hourlyForecast.hour_ts * 1000 >= now)
    .slice(0, 24)
    .map((hourlyForecast: IYandexHourForecastRes) => {
      const { hour_ts, temp, condition } = hourlyForecast;

      return {
        date: hour_ts * 1000,
        temp,
        condition: condition,
      };
    });
};

const getYandexWeather = (coords: ICoords): Promise<IWeatherData> => {
  return getWeather(coords).then((response: IYandexApiResponseData) => {
    const weatherData: IWeatherData = {
      current: getCurrentWeatherData(response),
      forecast: getForecastData(response.forecasts),
      hourly: getHourlyWeatherData(response.forecasts),
    };

    return weatherData;
  });
};

export { getCurrentWeatherData, getForecastData, getHourlyWeatherData, getYandexWeather };
