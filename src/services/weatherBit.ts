import {
  IWeatherBitCurrentRes,
  IWeatherBitForecastRes,
  IWeatherBitHourlyRes,
} from '../api/weatherBit/responseType';
import { IDailyWeather, IWeatherForecast, IHourlyWeather } from '../api/types';
import { parseDate } from './utils';
import { ICoords } from '../pages/Main/Main';
import { IWeatherData } from '../store/types';
import { getWeather } from '../api/weatherBit';
import { IClimacellApiRes } from '../api/climacell/responseType';
import { AxiosResponse } from 'axios';

const getCurrentWeatherData = (response: IWeatherBitCurrentRes): IDailyWeather => {
  const { data } = response;
  const { sunset, sunrise } = parseDate(new Date().getTime(), data[0].sunset, data[0].sunrise);

  return {
    temperature: Math.round(data[0].temp),
    humidity: Math.round(data[0].rh),
    pressure: Math.round(data[0].pres),
    feelsLike: Math.round(data[0].app_temp),
    condition: data[0].weather.description.toLowerCase(),
    sunset,
    sunrise,
  };
};

const getForecastData = (response: IWeatherBitForecastRes): IWeatherForecast[] => {
  const { data } = response;
  const forecastData: IWeatherForecast[] = data.map((forecast) => {
    return {
      date: forecast.ts * 1000,
      tempMax: Math.round(forecast.max_temp),
      tempMin: Math.round(forecast.min_temp),
      condition: forecast.weather.description.toLowerCase(),
    };
  });

  return forecastData;
};

const getHourlyWeatherData = (response: IWeatherBitHourlyRes): IHourlyWeather[] => {
  const { data } = response;
  const hourlyWeatherData: IHourlyWeather[] = data.slice(0, 24).map((hourlyWeather) => {
    return {
      date: hourlyWeather.ts * 1000,
      temp: Math.round(hourlyWeather.temp),
      condition: hourlyWeather.weather.description.toLowerCase(),
    };
  });

  return hourlyWeatherData;
};

const getWBitWeather = (coords: ICoords): Promise<IWeatherData> => {
  return getWeather(coords).then((response: AxiosResponse[]) => {
    const data = response.map((res: AxiosResponse) => res.data);
    const weatherData: IWeatherData = {
      current: getCurrentWeatherData(data[0] as IWeatherBitCurrentRes),
      forecast: getForecastData(data[1] as IWeatherBitForecastRes),
      hourly: getHourlyWeatherData(data[2] as IWeatherBitHourlyRes),
    };

    return weatherData;
  });
};

export { getCurrentWeatherData, getForecastData, getHourlyWeatherData, getWBitWeather };
