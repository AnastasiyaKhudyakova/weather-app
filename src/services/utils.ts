const parseDate = (
  date: number,
  sunset: string,
  sunrise: string,
): { sunset: number; sunrise: number } => {
  try {
    const sunsetTimestamp = convertToTimeStamp(date, sunset);
    const sunriseTimestamp = convertToTimeStamp(date, sunrise);

    return {
      sunset: sunsetTimestamp,
      sunrise: sunriseTimestamp,
    };
  } catch (error) {
    return {
      sunset: 0,
      sunrise: 0,
    };
  }
};

// converts string "HH:MM" to timestamp
const convertToTimeStamp = (date: number, time: string): number => {
  const [hours, min] = time.split(':');
  const day = new Date(date);
  const convertedDay = new Date(day.getFullYear(), day.getMonth(), day.getDate(), +hours, +min);

  return convertedDay.getTime();
};

export { parseDate, convertToTimeStamp };
