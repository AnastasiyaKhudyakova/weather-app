import { IDailyWeather, IHourlyWeather, IWeatherForecast } from '../api/types';
import {
  IOpenMapCurrentWeatherRes,
  IOpenMapDailyWeatherRes,
  IOpenMapHourlyRes,
  IOpenMapApiRes,
} from '../api/weatherMap/responseType';
import { ICoords } from '../pages/Main/Main';
import { IWeatherData } from '../store/types';
import { getWeather } from '../api/weatherMap';

const getCurrentWeatherData = (currentWeather: IOpenMapCurrentWeatherRes): IDailyWeather => {
  const { temp, humidity, pressure, feels_like, weather, sunset, sunrise } = currentWeather;

  return {
    temperature: Math.ceil(temp),
    humidity: humidity,
    pressure: Math.ceil(pressure),
    feelsLike: Math.ceil(feels_like),
    condition: weather[0].main.toLowerCase(),
    sunset: sunset * 1000,
    sunrise: sunrise * 1000,
  };
};

const getHourlyWeatherData = (hourlyWeather: IOpenMapHourlyRes[]): IHourlyWeather[] => {
  return hourlyWeather.slice(0, 24).map((hourlyForecast: IOpenMapHourlyRes) => {
    const { dt, temp, weather } = hourlyForecast;

    return {
      date: dt * 1000,
      temp: Math.ceil(temp),
      condition: weather[0].main.toLowerCase(),
    };
  });
};

const getForecastData = (dailyWeather: IOpenMapDailyWeatherRes[]): IWeatherForecast[] => {
  const forecast = dailyWeather.map((dailyData: IOpenMapDailyWeatherRes) => {
    const { dt, weather, temp } = dailyData;
    const dailyForecast: IWeatherForecast = {
      date: dt * 1000,
      tempMax: Math.ceil(temp.max),
      tempMin: Math.floor(temp.min),
      condition: weather[0].main.toLowerCase(),
    };

    return dailyForecast;
  });

  return forecast;
};

const getOpenMapWeather = (coords: ICoords): Promise<IWeatherData> => {
  return getWeather(coords).then((response: IOpenMapApiRes) => {
    const weatherData: IWeatherData = {
      current: getCurrentWeatherData(response.current),
      forecast: getForecastData(response.daily),
      hourly: getHourlyWeatherData(response.hourly),
    };

    return weatherData;
  });
};

export { getCurrentWeatherData, getForecastData, getHourlyWeatherData, getOpenMapWeather };
