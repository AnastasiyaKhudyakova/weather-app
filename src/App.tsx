import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import './App.less';
import Navbar from './components/Navbar/Navbar';
import DetailPage from './pages/Detail/Detail';
import LoginPage from './pages/Login/Login';
import Main, { ICoords } from './pages/Main/Main';
import NotFound from './pages/NotFound/NotFound';
import { getAuthUserOnLoad } from './store/auth/actions';
import { IAuthState } from './store/auth/reducer';
import { selectAuthState } from './store/auth/selectors';
import { RootState } from './store/rootReducer';

export enum WeatherApis {
  CLIMACELL = 'climacell',
  WEATHER_MAP = 'weatherMap',
  WEATHER_BIT = 'weatherBit',
  YANDEX = 'yandex',
}

export const WEATHER_APIS: string[] = Object.values(WeatherApis);

const App: FC = () => {
  const { user, isLoading } = useSelector<RootState, IAuthState>(selectAuthState);
  const dispatch = useDispatch();

  const routes = WEATHER_APIS.map((serviceName: string) => {
    return (
      <Route key={`route-${serviceName}`} path={`/${serviceName}`}>
        <DetailPage />
      </Route>
    );
  });

  useEffect(() => {
    dispatch(getAuthUserOnLoad());
  }, []);

  return (
    <Router>
      <div className='App'>
        <Navbar />
        {!isLoading && (
          <Switch>
            <Route exact path='/'>
              {user ? <Main /> : <Redirect to={{ pathname: '/login' }} />}
            </Route>

            {routes}

            <Route path='/login'>
              {!user ? <LoginPage /> : <Redirect to={{ pathname: '/' }} />}
            </Route>

            <Route path='/404'>
              <NotFound />
            </Route>
            <Route path='*'>
              <Redirect to={{ pathname: '/404' }} />
            </Route>
          </Switch>
        )}
      </div>
    </Router>
  );
};

export default App;
