import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { ICoords } from '../Main/Main';
import './Detail.less';

const DetailPage: React.FC = () => {
  const location = useLocation();
  const serviceName = location.pathname.slice(1);

  const { t } = useTranslation();

  // const { user, isLoading } = useSelector<RootState, IAuthState>(selectAuthState);

  const dispacth = useDispatch();
  const fetchWeather = () => {
    // if (weatherApiMap[serviceName] && position?.lon && position?.lat) {
    //   dispacth(weatherApiMap[serviceName](position));
    // }
  };

  useEffect(() => {
    fetchWeather();
  }, [location.pathname]);

  return null;
  // if (!isLoading && !user) {
  //   return <Redirect to={{ pathname: '/login' }} />;
  // }

  // return (
  //   <>
  //     {isFetching && <IconLoader />}
  //     {!isFetching && (
  //       <>
  //         <LocationInfo coords={position} />
  //         <div className='detail__forecast_page'>
  //           {weatherData?.current && (
  //             <div className='detail__forecast_current'>
  //               {/*<CurrentWeatherCard
  //                 fetchWeather={fetchWeather}
  //                 weatherData={weatherData.current}
  //                 logo={`/images/${serviceName}.png`}
  //               /> */}
  //             </div>
  //           )}
  //           {weatherData?.forecast.length && (
  //             <>
  //               <h3 className='main__subtitle'>{t('weather_hourly')}</h3>
  //               <div className='service__forecast_hourly'>
  //                 <ForecastChart hourlyForecast={weatherData.hourly} />
  //               </div>
  //             </>
  //           )}

  //           {weatherData?.forecast.length && (
  //             <>
  //               <h3 className='main__subtitle'>{t('weather_daily')}</h3>
  //               <div className='service__forecast_daily'>
  //                 <Forecast forecastWeather={weatherData.forecast} />
  //               </div>
  //             </>
  //           )}
  //         </div>
  //       </>
  //     )}
  //   </>
  // );
};

export default DetailPage;
