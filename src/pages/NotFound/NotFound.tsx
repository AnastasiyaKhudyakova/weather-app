import React from 'react';
import WeatherIcon from '../../components/Icon/WeatherIcon';
import './NotFound.less';
import { useTranslation } from 'react-i18next';

const NotFound: React.FC = () => {
  const { t } = useTranslation();

  return (
    <div className='notFound__error_container'>
      <h3 className='notFound__error_code'>404</h3>
      <div className='notFound__error_icon'>
        <WeatherIcon condition='cloudy' />
      </div>
      <h4 className='notFound__error_message'>{t('not found')}</h4>
    </div>
  );
};

export default NotFound;
