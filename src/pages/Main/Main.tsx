import { Tabs } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import WeatherTab from '../../components/WeatherTab/WeatherTab';
import { RootState } from '../../store/rootReducer';
import { IUserTabConfigs } from '../../store/userConfig/reducer';
import { selectUserConfig } from '../../store/userConfig/selectors';
import './Main.less';
import { addTab, removeTab, refreshTab } from '../../store/userConfig/types';
import { removeWeatherTab, addWeatherTab } from '../../store/userConfig/actions';

const { TabPane } = Tabs;

export interface ICoords {
  lat: number;
  lon: number;
}

const Main: React.FC = (): React.ReactElement => {
  const config = useSelector<RootState, IUserTabConfigs[]>(selectUserConfig);

  const dispatch = useDispatch();

  const tabs = config.map((tabConfig: IUserTabConfigs, index) => {
    return (
      <TabPane tab={tabConfig.city} key={`${tabConfig.city}-${index}`}>
        <WeatherTab tabValue={tabConfig} tabIndex={index} />
      </TabPane>
    );
  });

  const handleChange = (
    event: React.MouseEvent | React.KeyboardEvent | string,
    action: 'add' | 'remove',
  ) => {
    if (action === 'remove') {
      const index = Number(event.toString().split('-').pop());

      dispatch(removeWeatherTab(index));
    } else {
      dispatch(addWeatherTab());
    }
  };

  // return <div>Draft</div>;

  return (
    <div>
      {config?.length > 0 && (
        <Tabs type='editable-card' onEdit={handleChange}>
          {tabs}
        </Tabs>
      )}
    </div>
  );
};

export default Main;
