import React from 'react';
import LoginForm from '../../components/Auth/Login/LoginForm';

const LoginPage: React.FC = (): React.ReactElement => {
  return <LoginForm />;
};

export default LoginPage;
