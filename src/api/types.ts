export interface IDailyWeather {
  temperature: number;
  humidity: number;
  pressure: number;
  feelsLike: number;
  condition: string;
  sunset: number;
  sunrise: number;
}

export interface IWeatherForecast {
  date: number;
  tempMax: number;
  tempMin: number;
  condition: string;
}

export interface IHourlyWeather {
  date: number;
  temp: number;
  condition: string;
}

export interface IBasicRequestConfig {
  baseURL: string;
  headers: {
    [key: string]: string | undefined;
  };
  params?: {
    [key: string]: string | string[] | boolean | number | undefined;
  };
}
