import axios from 'axios';
import { IBasicRequestConfig } from '../types';
import { IYandexApiResponseData } from './responseType';
import { ICoords } from '../../pages/Main/Main';

const getRequestConfig = (coords: ICoords): IBasicRequestConfig => {
  return {
    baseURL: 'api/yandex',
    headers: {
      'Content-Type': 'application/json',
      'X-Yandex-API-Key': process.env.REACT_APP_YANDEX_WEATHER_API_KEY,
    },
    params: { lon: coords.lon, lat: coords.lat },
  };
};

const getWeather = async (coords: ICoords): Promise<IYandexApiResponseData> => {
  const config: IBasicRequestConfig = getRequestConfig(coords);
  const response = await axios.request(config);

  return response.data;
};

export { getWeather };
