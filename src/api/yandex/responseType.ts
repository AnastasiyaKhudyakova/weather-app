export interface IYandexCurrentWeatherRes {
  condition: string;
  daytime: string;
  feels_like: number;
  humidity: number;
  icon: string;
  obs_time: number;
  polar: boolean;
  pressure_mm: number;
  pressure_pa: number;
  season: string;
  soil_moisture: number;
  soil_temp: number;
  source: string;
  temp: number;
  uv_index: number;
  wind_dir: string;
  wind_gust: number;
  wind_speed: number;
}

export interface IYandexForecastsRes {
  date: string;
  date_ts: number;
  hours: IYandexHourForecastRes[];
  moon_code: number;
  moon_text: string;
  parts: { [key: string]: IYandexPartsForecastsRes };
  rise_begin: string;
  set_end: string;
  sunrise: string;
  sunset: string;
  week: number;
}

export interface IYandexHourForecastRes {
  hour: string;
  hour_ts: number;
  temp: number;
  feels_like: number;
  icon: string;
  condition: string;
  wind_speed: number;
  wind_gust: number;
  wind_dir: string;
  pressure_mm: number;
  pressure_pa: number;
  humidity: number;
  uv_index: number;
  soil_temp: number;
  soil_moisture: number;
  prec_mm: number;
  prec_period: number;
  prec_prob: number;
}

export interface IYandexPartsForecastsRes {
  temp_min: number;
  temp_max: number;
  temp_avg: number;
  feels_like: number;
  icon: string;
  condition: string;
  daytime: string;
  polar: boolean;
  wind_speed: number;
  wind_dir: string;
  pressure_mm: number;
  pressure_pa: number;
  humidity: number;
  uv_index: number;
  soil_temp: number;
  soil_moisture: number;
  prec_mm: number;
  prec_period: number;
  prec_prob: number;
}

export interface IYandexWeatherInfo {
  def_pressure_mm: number;
  def_pressure_pa: number;
  f: boolean;
  geoid: number;
  lat: number;
  lon: number;
  n: boolean;
  nr: boolean;
  ns: boolean;
  nsr: boolean;
  p: boolean;
  slug: string;
  tzinfo: {
    abbr: string;
    dst: string;
    name: string;
    offset: number;
  };
  url: string;
  zoom: number;
  _h: boolean;
}

export interface IYandexApiResponseData {
  now: number;
  fact: IYandexCurrentWeatherRes;
  forecasts: IYandexForecastsRes[];
  info: IYandexWeatherInfo;
}
