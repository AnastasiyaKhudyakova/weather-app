import axios, { AxiosResponse } from 'axios';
import { ICoords } from '../../pages/Main/Main';
import { IBasicRequestConfig } from '../types';

const WeatherBitEndpoints: { [key: string]: string } = {
  current: 'https://api.weatherbit.io/v2.0/current',
  forecast: 'https://api.weatherbit.io/v2.0/forecast/daily',
  hourly: 'https://api.weatherbit.io/v2.0/forecast/hourly',
};

const getRequestConfig = (coords: ICoords, urlAlias: string): IBasicRequestConfig => {
  return {
    baseURL: WeatherBitEndpoints[urlAlias],
    headers: {
      'Content-type': 'application/json',
    },
    params: {
      key: process.env.REACT_APP_WEATHERBIT_API_KEY,
      lon: coords.lon,
      lat: coords.lat,
    },
  };
};

const getWeather = async (coords: ICoords): Promise<AxiosResponse[]> => {
  const currentReqConfig: IBasicRequestConfig = getRequestConfig(coords, 'current');
  const forecastReqConfig: IBasicRequestConfig = getRequestConfig(coords, 'forecast');
  const hourlyReqConfig: IBasicRequestConfig = getRequestConfig(coords, 'hourly');

  const response = await Promise.all([
    axios.request(currentReqConfig),
    axios.request(forecastReqConfig),
    axios.request(hourlyReqConfig),
  ]);

  return response;
};

export { getWeather };
