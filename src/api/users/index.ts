import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { firebaseConfig } from '../../fb.config';

export interface IUserReqParams {
  uid: string;
  idToken: string;
  data?: { [key: string]: any };
}

const getUser = async (reqParams: IUserReqParams): Promise<AxiosResponse> => {
  const { uid, idToken } = reqParams;
  const reqConfig: AxiosRequestConfig = {
    headers: {
      'content-type': 'application/json',
    },
    params: {
      auth: idToken,
    },
  };

  const response = await axios.get(`${firebaseConfig.databaseURL}/users/${uid}.json`, reqConfig);

  return response;
};

const updateUser = async (reqParams: IUserReqParams): Promise<AxiosResponse> => {
  const { uid, idToken, data } = reqParams;
  const reqConfig: AxiosRequestConfig = {
    headers: {
      'content-type': 'application/json',
    },
    params: {
      auth: idToken,
    },
  };

  const response = await axios.put(
    `${firebaseConfig.databaseURL}/users/${uid}.json`,
    data,
    reqConfig,
  );

  return response;
};

export { getUser, updateUser };
