import axios from 'axios';
import qs from 'qs';
import { IBasicRequestConfig } from '../types';
import { IClimacellApiRes } from './responseType';
import { ICoords } from '../../pages/Main/Main';

export interface IClimcellRequestConfig extends IBasicRequestConfig {
  paramsSerializer: (params: { [key: string]: string | string[] }) => string;
}

const getRequestConfig = (coords: ICoords): IClimcellRequestConfig => {
  return {
    baseURL: 'https://api.climacell.co/v3/weather/forecast/hourly',
    headers: {
      'Content-Type': 'application/json',
      apikey: process.env.REACT_APP_CLIMA_CELL_API_KEY,
    },
    params: {
      fields: [
        'temp',
        'feels_like',
        'humidity',
        'wind_speed',
        'baro_pressure',
        'weather_code',
        'sunrise',
        'sunset',
      ],
      start_time: 'now',
      lon: coords.lon,
      lat: coords.lat,
    },
    paramsSerializer: (params: { [key: string]: string | string[] }) =>
      qs.stringify(params, { arrayFormat: 'repeat' }),
  };
};

const getWeather = async (coords: ICoords): Promise<IClimacellApiRes[]> => {
  const config: IClimcellRequestConfig = getRequestConfig(coords);
  const response = await axios.request(config);
  const { data } = response;

  return data;
};

export { getWeather };
