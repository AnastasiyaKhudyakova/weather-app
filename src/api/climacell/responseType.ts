export interface IClimacellApiRes {
  lat: number;
  lon: number;
  temp: {
    value: number;
    units: string;
  };
  feels_like: {
    value: number;
    units: string;
  };
  humidity: {
    value: number;
    units: string;
  };
  baro_pressure: {
    value: number;
    units: string;
  };
  wind_speed: {
    value: number;
    units: string;
  };
  sunrise: {
    value: string;
  };
  sunset: {
    value: string;
  };
  weather_code: {
    value: string;
  };
  observation_time: {
    value: string;
  };
}
