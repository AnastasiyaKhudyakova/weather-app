export interface ICitiesDBCityData {
  id: number;
  wikiDataId: string;
  type: string;
  city: string;
  name: string;
  country: string;
  countryCode: string;
  region: string;
  regionCode: string;
  latitude: number;
  longitude: number;
}

export interface ICitiesDBResponse {
  data: ICitiesDBCityData[];
  metadata: {
    currentOffset: number;
    totalCount: number;
  };
  links?: { rel: string; href: string }[];
}
