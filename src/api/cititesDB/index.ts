import axios from 'axios';
import { IBasicRequestConfig } from '../types';
import { ICitiesDBResponse } from './responseType';

const getRequestConfig = (query: string, lang: string): IBasicRequestConfig => {
  return {
    baseURL: 'https://wft-geo-db.p.rapidapi.com/v1/geo/cities',
    headers: {
      'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
      'x-rapidapi-key': process.env.REACT_APP_RAPID_API_KEY,
      'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
    },
    params: {
      namePrefix: createQueryString(query),
      languageCode: lang,
      limit: 10,
    },
  };
};

const getCitiesByQuery = async (
  query: string,
  languageCode: string,
): Promise<ICitiesDBResponse> => {
  const config: IBasicRequestConfig = getRequestConfig(query, languageCode);
  const response = await axios.request(config);

  return response.data;
};

// creates query string for compound city names
const createQueryString = (query: string): string => {
  const queryString = query.trim().toLocaleLowerCase().replace(/\s+/g, '+');

  return queryString;
};

export { getCitiesByQuery };
