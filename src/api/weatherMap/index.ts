import axios from 'axios';
import { IBasicRequestConfig } from '../types';
import { IOpenMapApiRes } from './responseType';
import { ICoords } from '../../pages/Main/Main';

const getRequestConfig = (lon: number, lat: number): IBasicRequestConfig => {
  return {
    baseURL: 'https://api.openweathermap.org/data/2.5/onecall',
    headers: {
      'Content-Type': 'application/json',
    },
    params: {
      units: 'metric',
      appid: process.env.REACT_APP_WEATHER_MAP_API_KEY,
      lon,
      lat,
    },
  };
};

const getWeather = async (coords: ICoords): Promise<IOpenMapApiRes> => {
  const { lon, lat } = coords;
  const config: IBasicRequestConfig = getRequestConfig(lon, lat);
  const response = await axios.request(config);
  const { data } = response;

  return data;
};

export { getWeather };
