import axios, { AxiosRequestConfig } from 'axios';
import { IBasicRequestConfig } from '../api/types';
import { IAuthResponseData } from './responseType';
import { firebaseConfig } from '../fb.config';

export interface IFirebaseAuthReqConfig extends IBasicRequestConfig {
  method: AxiosRequestConfig['method'];
  body?: string;
}

export interface IUserCredentials {
  email: string;
  password: string;
}

export interface IUserAuthData extends IUserCredentials {
  isSignUp: boolean;
}

const getRequestConfig = (authData: IUserAuthData): IFirebaseAuthReqConfig => {
  const config: IFirebaseAuthReqConfig = {
    baseURL: authData.isSignUp ? firebaseConfig.signUpUrl : firebaseConfig.loginUrl,
    headers: {
      'Content-type': 'application/json',
    },
    params: {
      key: process.env.REACT_APP_AUTH_API_KEY,
      email: authData.email,
      password: authData.password,
      returnSecureToken: true,
    },
    method: 'POST',
  };

  return config;
};

const signIn = async (authData: IUserAuthData): Promise<IAuthResponseData> => {
  const reqConfig = getRequestConfig(authData);
  const response = await axios.request(reqConfig);

  return response.data;
};

export { signIn };
